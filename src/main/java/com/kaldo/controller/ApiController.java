package com.kaldo.controller;

import com.kaldo.model.Resource;
import com.kaldo.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiController {

	private final ResourceService resourceService;

	@Autowired
	public ApiController(ResourceService resourceService) {
		this.resourceService = resourceService;
	}

	@RequestMapping("/resources")
	public List<Resource> getResourcesJSON(){
		return resourceService.getAllResources();
	}

}
