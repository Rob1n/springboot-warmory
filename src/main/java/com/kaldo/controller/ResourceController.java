package com.kaldo.controller;

import com.kaldo.model.Resource;
import com.kaldo.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/items/resources")
public class ResourceController {


	private final ResourceService resourceService;

	@Autowired
	public ResourceController(ResourceService resourceService) {
		this.resourceService = resourceService;
	}

	@RequestMapping()
	public String listResources(Model model){
		model.addAttribute("resources", resourceService.getAllResources());
		return "resources";
	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String addResourceForm(Model model){
		model.addAttribute("resource", new Resource());
		return "resources-new";
	}

	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public String createResource(@ModelAttribute("resource") Resource resource, BindingResult result){
		if (result.hasErrors()){
			System.out.println(result);
			return "resources-new";
		}

		resourceService.createResource(resource);
		return "resources";
	}



}
