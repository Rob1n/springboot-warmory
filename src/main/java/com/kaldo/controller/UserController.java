package com.kaldo.controller;

import com.kaldo.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getLoginPage(Model model){
		model.addAttribute("user", new User());
		return "login";
	}

	@PostMapping("/users/create")
	public String createUser(){
		return "login";
	}

}
