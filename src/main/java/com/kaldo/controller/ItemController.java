package com.kaldo.controller;

import com.kaldo.model.Equipment;
import com.kaldo.model.Long;
import com.kaldo.service.EquipmentService;
import com.kaldo.service.ItemService;
import com.kaldo.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/items")
public class ItemController {

	private final EquipmentService equipmentService;
	private final ItemService itemService;
	private final ResourceService resourceService;

	@Autowired
	public ItemController(EquipmentService equipmentService, ItemService itemService, ResourceService resourceService) {
		this.equipmentService = equipmentService;
		this.itemService = itemService;
		this.resourceService = resourceService;
	}

	@RequestMapping()
	public String itemsDefault(Model model){
		List<Long> allItems = new ArrayList<>();
		allItems.addAll(equipmentService.getAllEquipment());
		model.addAttribute("items", allItems);
		return "items";
	}

	@RequestMapping("/equipment")
	public String equipmentList(Model model){
		List<Equipment> allEquipments = new ArrayList<>();
		allEquipments.addAll(equipmentService.getAllEquipment());
		model.addAttribute("equipment", allEquipments);
		return "equipment";
	}

	@RequestMapping(value = "/equipment/new", method = RequestMethod.GET)
	public String addNewEquipmentGet(Model model){
		model.addAttribute("equipment", new Equipment());
		model.addAttribute("allItems", itemService.getAllItems());
		model.addAttribute("allResources", resourceService.getAllResources());
		return "equipment-new";
	}

	@RequestMapping(value = "/equipment/new", method = RequestMethod.POST)
	public String createNewItem(@Valid @ModelAttribute("equipment") Equipment equipment, BindingResult result,
								HttpServletRequest request){
		if (result.hasErrors()){
			System.out.println(result);
			return "equipment-new";
		}

		List<String> rComp = new ArrayList<>();
		String tmp;
		try {
			if (!(tmp = request.getParameter("res1")).isEmpty()){
				rComp.add(tmp);
			}
		} catch (Exception e){}

		try {
			if (!(tmp = request.getParameter("res2")).isEmpty()){
				rComp.add(tmp);
			}
		} catch (Exception e){}

		try {
			if (!(tmp = request.getParameter("res3")).isEmpty()){
				rComp.add(tmp);
			}
		} catch (Exception e) {}

		try {
			if (!(tmp = request.getParameter("res4")).isEmpty()) {
				rComp.add(tmp);
			}
		} catch (Exception e) {}

		System.out.println("stst");
		equipmentService.createEquipment(equipment, rComp);
		return "redirect:/items/equipment.html";
	}

	@RequestMapping(value = "/{itemId}", method = RequestMethod.GET)
	public String showItem(@PathVariable("itemId") java.lang.Long itemId, Model model){
		model.addAttribute("item", equipmentService.getEquipment(itemId));
		return "equipment-detail";
	}

	@RequestMapping(value = "/{itemId}/delete", method = RequestMethod.GET)
	public String deleteEquipment(@PathVariable("itemId") java.lang.Long equipmentId){
		equipmentService.deleteEquipment(equipmentId);
		return "redirect:/items/equipment.html";
	}

}
