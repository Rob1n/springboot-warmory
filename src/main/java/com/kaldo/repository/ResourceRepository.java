package com.kaldo.repository;

import com.kaldo.model.Resource;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ResourceRepository extends JpaRepository<Resource, Long> {
	public List<Resource> findAllByCode(String code);
}
