package com.kaldo.service;

import com.kaldo.model.Resource;

import java.util.List;

public interface ResourceService {
	List<Resource> getAllResources();

	Resource createResource(Resource resource);

	List<Resource> getResourcesByCode(String code);
}
