package com.kaldo.service;

import com.kaldo.model.Long;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

	private final ResourceService resourceService;
	private final EquipmentService equipmentService;

	@Autowired
	public ItemServiceImpl(ResourceService resourceService, EquipmentService equipmentService) {
		this.resourceService = resourceService;
		this.equipmentService = equipmentService;
	}

	@Override
	public List<Long> getAllItems(){
		ArrayList<Long> items = new ArrayList<>();
		items.addAll(equipmentService.getAllEquipment());
		items.addAll(resourceService.getAllResources());
		return items;
	}

}
