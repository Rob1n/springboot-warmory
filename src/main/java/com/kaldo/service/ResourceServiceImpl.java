package com.kaldo.service;

import com.kaldo.model.Resource;
import com.kaldo.repository.ResourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResourceServiceImpl implements ResourceService {

	private final ResourceRepository resourceRepository;

	@Autowired
	public ResourceServiceImpl(ResourceRepository resourceRepository) {
		this.resourceRepository = resourceRepository;
	}

	@Override
	public List<Resource> getAllResources(){
		return resourceRepository.findAll();
	}

	@Override
	public Resource createResource(Resource resource){
		return resourceRepository.saveAndFlush(resource);
	}

	@Override
	public List<Resource> getResourcesByCode(String code){
		return resourceRepository.findAllByCode(code);
	}

}
