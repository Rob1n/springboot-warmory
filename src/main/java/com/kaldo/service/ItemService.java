package com.kaldo.service;

import com.kaldo.model.Long;

import java.util.List;

public interface ItemService {
	List<Long> getAllItems();
}
