package com.kaldo.service;

import com.kaldo.model.Equipment;
import com.kaldo.model.Long;

import java.util.List;

public interface EquipmentService {
	List<Equipment> getAllEquipment();

	Equipment createEquipment(Equipment equipment, List<String> resourcesUsed);

	Equipment getEquipment(java.lang.Long id);

	List<Long> getComponents(java.lang.Long id);

	Equipment deleteEquipment(java.lang.Long id);
}
