package com.kaldo.service;

import com.kaldo.model.Equipment;
import com.kaldo.model.Long;
import com.kaldo.model.Resource;
import com.kaldo.repository.EquipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EquipmentServiceImpl implements EquipmentService {

	private final EquipmentRepository equipmentRepository;
	private final ResourceService resourceService;

	@Autowired
	public EquipmentServiceImpl(EquipmentRepository equipmentRepository, ResourceService resourceService) {
		this.equipmentRepository = equipmentRepository;
		this.resourceService = resourceService;
	}

	@Override
	public List<Equipment> getAllEquipment(){
		return equipmentRepository.findAll();
	}

	@Override
	public Equipment createEquipment(Equipment equipment, List<String> resourcesUsed){

		//TODO stuff

		List<Resource> resources = new ArrayList<>();
		for (String res : resourcesUsed){
			resources.add(resourceService.getResourcesByCode(res).get(0));
		}
		equipment.setComponentsResources(resources);

		return equipmentRepository.saveAndFlush(equipment);
	}

	@Override
	public Equipment getEquipment(java.lang.Long id) {
		return equipmentRepository.getOne(id);
	}

	@Override
	public List<Long> getComponents(java.lang.Long id){
		Equipment eq = getEquipment(id);
		List<Long> components = new ArrayList<>();
		components.addAll(eq.getComponentsEquipment());
		components.addAll(eq.getComponentsResources());
		return components;
	}

	@Override
	public Equipment deleteEquipment(java.lang.Long id){
		Equipment tEq = equipmentRepository.getOne(id);
		equipmentRepository.delete(tEq);
		return tEq;
	}

}
