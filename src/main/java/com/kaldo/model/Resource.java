package com.kaldo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "resources")
public class Resource extends Long {

	@ManyToMany(mappedBy = "componentsResources", targetEntity = Equipment.class)
	@JsonIgnore
	private List<Equipment> usedInEquipment = new ArrayList<>();

	@JsonIgnore
	public List<Equipment> getUsedInEquipment() {
		return usedInEquipment;
	}

	@JsonIgnore
	public void setUsedInEquipment(List<Equipment> usedInEquipment) {
		this.usedInEquipment = usedInEquipment;
	}
}
