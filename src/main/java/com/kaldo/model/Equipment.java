package com.kaldo.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "equipment")
public class Equipment extends Long {

	private String description;
	private String slot;
	private Boolean prime;

	// resources
	@ManyToMany(targetEntity = Resource.class, fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	private List<Resource> componentsResources = new ArrayList<>();

	@Transient
	private Long component1, component2, component3, component4;

	// eq comps
	@ManyToMany(targetEntity = Equipment.class, cascade = CascadeType.PERSIST)
	private List<Equipment> componentsEquipment = new ArrayList<>();

	@ManyToMany(targetEntity = Equipment.class, mappedBy = "componentsEquipment")
	private List<Equipment> usedInEquipment = new ArrayList<>();


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSlot() {
		return slot;
	}

	public void setSlot(String slot) {
		this.slot = slot;
	}

	public Boolean getPrime() {
		return prime;
	}

	public void setPrime(Boolean prime) {
		this.prime = prime;
	}

	public Long getComponent1() {
		return component1;
	}

	public void setComponent1(Long component1) {
		this.component1 = component1;
	}

	public Long getComponent2() {
		return component2;
	}

	public void setComponent2(Long component2) {
		this.component2 = component2;
	}

	public Long getComponent3() {
		return component3;
	}

	public void setComponent3(Long component3) {
		this.component3 = component3;
	}

	public Long getComponent4() {
		return component4;
	}

	public void setComponent4(Long component4) {
		this.component4 = component4;
	}

	public List<Resource> getComponentsResources() {
		return componentsResources;
	}

	public void setComponentsResources(List<Resource> componentsResources) {
		this.componentsResources = componentsResources;
	}

	public List<Equipment> getComponentsEquipment() {
		return componentsEquipment;
	}

	public void setComponentsEquipment(List<Equipment> componentsEquipment) {
		this.componentsEquipment = componentsEquipment;
	}

	public List<Equipment> getUsedInEquipment() {
		return usedInEquipment;
	}

	public void setUsedInEquipment(List<Equipment> usedInEquipment) {
		this.usedInEquipment = usedInEquipment;
	}
}
