package com.kaldo.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@MappedSuperclass
public abstract class Long {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected java.lang.Long id;

	@NotNull
	@Length(min=3, max=20)
	protected String code;

	@NotNull
	@Length(min=3, max=100)
	protected String name;



	public java.lang.Long getId() {
		return id;
	}

	public void setId(java.lang.Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
