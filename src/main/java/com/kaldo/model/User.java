package com.kaldo.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

//@Entity
//@Table(name = "users")
public class User {

	@Id
	@NotNull
	private Long id;

	@NotNull
	private String username;
	private String email;

	@Length(min = 4, max = 40)
	private String password;
	@Transient
	private String passwordConfirm;

	@Transient
	private Boolean rememberLogin;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public Boolean getRememberLogin() {
		return rememberLogin;
	}

	public void setRememberLogin(Boolean rememberLogin) {
		this.rememberLogin = rememberLogin;
	}
}
