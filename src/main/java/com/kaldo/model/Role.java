package com.kaldo.model;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

//@Entity
//@Table(name = "roles")
public class Role {

	@Id
	@NotNull
	private Long id;

	private RoleEnum name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RoleEnum getName() {
		return name;
	}

	public void setName(RoleEnum name) {
		this.name = name;
	}
}
