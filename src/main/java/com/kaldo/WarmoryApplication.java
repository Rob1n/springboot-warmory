package com.kaldo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WarmoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(WarmoryApplication.class, args);
	}
}
