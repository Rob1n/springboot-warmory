CREATE TABLE IF NOT EXISTS weapons (
  id          BIGINT AUTO_INCREMENT PRIMARY KEY,
  code        VARCHAR(255),
  name        VARCHAR(255),
  description VARCHAR(255),
  slot        BIGINT,
  prime       TINYINT,
  component1  BIGINT,
  component2  BIGINT,
  component3  BIGINT,
  component4  BIGINT,
  comp1qt     INT,
  comp2qt     INT,
  comp3qt     INT,
  comp4qt     INT
);

CREATE TABLE IF NOT EXISTS resources (
  id   BIGINT AUTO_INCREMENT PRIMARY KEY,
  code VARCHAR(255),
  name VARCHAR(255)
);

