var cont = document.getElementById("dynamicFormContainerLeft");
var numberOfAddedRows = 2;

function addLeftTableRow() {
    if (numberOfAddedRows > 4)
        return;
    console.log("add row " + numberOfAddedRows);

    var rowSelectId = "row" + numberOfAddedRows;
    var lastEditedSelect = rowSelectId;

    newHtml = '<div class="row">';

    // column 1
    newHtml += '<div class="col-md-2"><label class="control-label">Item '+numberOfAddedRows+'</label></div>';

    // column 2
    newHtml += '<div class="col-md-4">';
    newHtml += '<select id="'+rowSelectId+'" name="res'+numberOfAddedRows+'">';
    // for (i=0; i<10; i++){
    //     newHtml += '<option value="AL'+numberOfAddedRows+'">Alabama' + i + '</option>';
    // }

    $.getJSON("http://localhost:8080/api/resources", {
        format: "json"
    })
        .done(function (data) {
            var selectTag = $("#"+lastEditedSelect);
            $.each(data, function (i, item) {
                var newState = new Option(item.name, item.code, true, true);
                // $("#"+lastEditedSelect).append(newState).trigger('change');
                // selectTag.select2('data', {value: item.code, text: item.name});
                selectTag.append('<option value="' + item.code + '" class="form-control">' + item.name + '</option>');
                selectTag.trigger('change');
            });
            // selectTag.select2();
        });


    // for (i=0; i<allResources.length; i++){
    //     newHtml += '<option value="'+allResources[i].code+'">' + allResources[i].name + '</option>';
    // }
    newHtml += '</select></div>';
    $("#"+rowSelectId).select2();

    // column 3
    newHtml += '<div class="col-md-1">';
    newHtml += '<button type="button" class="btn btn-default" onclick="removeLeftTableRow(this)"><i class="fa fa-minus"></i></button>';
    newHtml += '</div>';

    // column 4
    newHtml += '<div class="col-md-5"><p>q:</p></div>';

    newHtml += '</div>';
    cont.innerHTML += newHtml;
    // $("#"+rowSelectId).select2();
    numberOfAddedRows++;
}

function removeLeftTableRow(o) {
    console.log("remove row");
    var p = o.parentNode.parentNode;
    p.parentNode.removeChild(p);
    numberOfAddedRows--;
}


(function(){
    console.log("Initialized");
    $(document).ready(function(){
        console.log("document ready!");
        // document.getElementsByClassName("addButton").item(0).oncl
    });
})();


$(document).ready(function() {
    $("#js-example-basic-single").select2();
});